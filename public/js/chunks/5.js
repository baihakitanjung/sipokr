(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Absen.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Absen.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue2-google-maps */ "./node_modules/vue2-google-maps/dist/main.js");
/* harmony import */ var vue2_google_maps__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      location: null,
      lat1: null,
      lng1: null,
      gettingLocation: false,
      errorStr: null,
      photo: "",
      submitted: false,
      center: {},
      // center: { lat: -6.315211, lng: 106.67632 },
      infoContent: "",
      infoWindowPos: null,
      infoWinOpen: false,
      //optional: offset infowindow so it visually sits nicely on top of our marker
      infoOptions: {
        pixelOffset: {
          width: 0,
          height: -35
        }
      },
      markers: [{
        // position: { lat: -6.315211, lng: 106.67632 },
        infoText: "Your" //   icon: gambar1

      } // {
      //   position: { lat: -6.294548, lng: 106.666249 },
      //   infoText: "Finish :" + new Date().toLocaleString(),
      //   //   icon: gambar2
      // }
      ],
      path: [// {
        //   lat: -6.315003,
        //   lng: 106.676188
        // },
        // { lat: -6.315211, lng: 106.67632 },
        // { lat: -6.314685, lng: 106.677152 },
        // { lat: -6.308236, lng: 106.673205 },
        // { lat: -6.306833, lng: 106.671569 },
        // { lat: -6.30521, lng: 106.669634 },
        // { lat: -6.304334, lng: 106.668917 },
        // { lat: -6.30321, lng: 106.668574 },
        // { lat: -6.301358, lng: 106.668121 },
        // { lat: -6.299366, lng: 106.667691 },
        // { lat: -6.297507, lng: 106.666871 },
        // { lat: -6.295747, lng: 106.666409 },
        // { lat: -6.295041, lng: 106.666321 },
        // { lat: -6.294548, lng: 106.666249 }
      ]
    };
  },
  computed: {
    google: vue2_google_maps__WEBPACK_IMPORTED_MODULE_0__["gmapApi"]
  },
  created: function created() {
    var _this = this;

    // do we support geolocation
    if (!("geolocation" in navigator)) {
      this.errorStr = 'Geolocation is not available.';
      return;
    }

    this.gettingLocation = true; // get position

    navigator.geolocation.getCurrentPosition(function (position1) {
      var lat = {
        lat: position1.coords.latitude
      };
      var lng = {
        lng: position1.coords.longitude
      };
      var pos = {
        lat: position1.coords.latitude,
        lng: position1.coords.longitude
      };

      _this.$refs.mapRef.$mapPromise.then(function (map) {
        map.panTo({
          lat: lat.lat,
          lng: lng.lng
        });
      });

      console.log(lat.lat);
      _this.lat1 = lat.lat;
      _this.lng1 = lng.lng;
      _this.gettingLocation = false;
      _this.location = pos;
      _this.center = _this.location;
      _this.markers[0].position = _this.location;
    }, function (err) {
      _this.gettingLocation = false;
      _this.errorStr = err.message;
    });
  },
  methods: {
    jikaSuccess: function jikaSuccess() {
      this.$vs.notify({
        title: "Pemberitahuan",
        text: "Berhasil Absen!!",
        iconPack: "feather",
        icon: "icon-success",
        color: "success",
        position: "top-right"
      });
    },
    jikaFailed: function jikaFailed() {
      this.$vs.notify({
        title: "Pemberitahuan",
        text: "Gagal Absen",
        iconPack: "feather",
        icon: "icon-x",
        color: "danger",
        position: "top-right"
      });
    },
    toggleInfoWindow: function toggleInfoWindow(marker, idx) {
      this.infoWindowPos = marker.position;
      this.infoContent = marker.infoText; // Jika Tanda ada yang sama maka beralih ke yang baru

      if (this.currentMidx == idx) {
        this.infoWinOpen = !this.infoWinOpen;
      } // jika marker berbeda maka mengatur infowindow dan mereset index marker saat ini
      else {
          this.infoWinOpen = true;
          this.currentMidx = idx;
        }
    },
    update_avatarKTP: function update_avatarKTP(input) {
      var _this2 = this;

      if (input.target.files && input.target.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          _this2.photo = e.target.result;
        };

        reader.readAsDataURL(input.target.files[0]);
      }
    },
    absen: function absen() {
      console.log(this.photo); //   console.log(this.location);

      console.log(this.lat1);
      console.log(this.lng1); // this.geolocation();
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".ukuran[data-v-4a11f33e] {\n  height: 250px;\n  width: 250px;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--7-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--7-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Absen.vue?vue&type=template&id=4a11f33e&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/src/views/Absen.vue?vue&type=template&id=4a11f33e&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "vx-row" }, [
    _c(
      "div",
      { staticClass: "vx-col w-full sm:w-1/2 md:w-1/2 mb-base custom" },
      [
        _c("vx-card", { staticClass: "p-2" }, [
          _c("div", { staticClass: "text-center" }, [
            _c("h4", [_vm._v("Your Photo")])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt-2" },
            [
              _c("viewer", { attrs: { toolbar: false } }, [
                _vm.photo
                  ? _c("img", {
                      staticClass:
                        "responsive rounded-lg ukuran mx-auto my-6 block",
                      attrs: { src: _vm.photo, alt: "content-img" }
                    })
                  : _c("img", {
                      staticClass:
                        "responsive rounded-lg ukuran mx-auto my-6 block",
                      attrs: {
                        src:
                          "https://s3.amazonaws.com/37assets/svn/765-default-avatar.png",
                        alt: "content-img"
                      }
                    })
              ]),
              _vm._v(" "),
              _c("input", {
                ref: "update_avatar_input1",
                staticClass: "hidden",
                attrs: { type: "file", accept: "image/*" },
                on: { change: _vm.update_avatarKTP }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "flex justify-center" },
                [
                  _c(
                    "vs-button",
                    {
                      staticClass: "mr-3",
                      attrs: {
                        type: "gradient",
                        color: "#7367F0",
                        "gradient-color-secondary": "#CE9FFC"
                      },
                      on: {
                        click: function($event) {
                          return _vm.$refs.update_avatar_input1.click()
                        }
                      }
                    },
                    [_vm._v("Change Photo")]
                  ),
                  _vm._v(" "),
                  _c(
                    "vs-button",
                    {
                      attrs: {
                        color: "success",
                        type: "gradient",
                        "gradient-color-secondary": "#CE9FFC"
                      },
                      on: { click: _vm.absen }
                    },
                    [_vm._v("Absen")]
                  )
                ],
                1
              )
            ],
            1
          )
        ])
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "vx-col w-full sm:w-1/2 md:w-1/2 mb-base" },
      [
        _c("vx-card", { staticClass: "p-2" }, [
          _c("div", { staticClass: "text-center" }, [
            _c("h4", [_vm._v("Your Location")])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "mt-2" },
            [
              _c(
                "gmap-map",
                {
                  ref: "mapRef",
                  staticStyle: { width: "100%", height: "500px" },
                  attrs: { center: _vm.center, position: _vm.google, zoom: 18 }
                },
                [
                  _c("gmap-info-window", {
                    attrs: {
                      options: _vm.infoOptions,
                      position: _vm.infoWindowPos,
                      opened: _vm.infoWinOpen
                    },
                    on: {
                      closeclick: function($event) {
                        _vm.infoWinOpen = false
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._l(_vm.markers, function(m, i) {
                    return _c("gmap-marker", {
                      key: i,
                      attrs: {
                        position: m.position,
                        clickable: true,
                        draggable: false,
                        icon: m.icon
                      },
                      on: {
                        click: function($event) {
                          return _vm.toggleInfoWindow(m, i)
                        }
                      }
                    })
                  })
                ],
                2
              )
            ],
            1
          )
        ])
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/src/views/Absen.vue":
/*!******************************************!*\
  !*** ./resources/js/src/views/Absen.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Absen_vue_vue_type_template_id_4a11f33e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Absen.vue?vue&type=template&id=4a11f33e&scoped=true& */ "./resources/js/src/views/Absen.vue?vue&type=template&id=4a11f33e&scoped=true&");
/* harmony import */ var _Absen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Absen.vue?vue&type=script&lang=js& */ "./resources/js/src/views/Absen.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Absen_vue_vue_type_style_index_0_id_4a11f33e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css& */ "./resources/js/src/views/Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Absen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Absen_vue_vue_type_template_id_4a11f33e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Absen_vue_vue_type_template_id_4a11f33e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "4a11f33e",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/src/views/Absen.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/src/views/Absen.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/src/views/Absen.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Absen.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Absen.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/src/views/Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/src/views/Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css& ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_style_index_0_id_4a11f33e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--7-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--7-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Absen.vue?vue&type=style&index=0&id=4a11f33e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_style_index_0_id_4a11f33e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_style_index_0_id_4a11f33e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_style_index_0_id_4a11f33e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_style_index_0_id_4a11f33e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_7_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_style_index_0_id_4a11f33e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/src/views/Absen.vue?vue&type=template&id=4a11f33e&scoped=true&":
/*!*************************************************************************************!*\
  !*** ./resources/js/src/views/Absen.vue?vue&type=template&id=4a11f33e&scoped=true& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_template_id_4a11f33e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Absen.vue?vue&type=template&id=4a11f33e&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/src/views/Absen.vue?vue&type=template&id=4a11f33e&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_template_id_4a11f33e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Absen_vue_vue_type_template_id_4a11f33e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);