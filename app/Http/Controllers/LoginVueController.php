<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\LoginVueModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginVueController extends Controller
{
    public function loginvue(Request $request)
    {

        $nik = $request->get('nik');

        $cek_user = DB::select("SELECT * FROM Sitama_Attendance.User where User.NIK = '$nik'");

        // dd(Hash::check($request->password, $cekpas));

        // dd($cek_user[0]->Password);
        if (empty($cek_user)) {
            $pesan = array('status' => 'Nik tidak ditemukan!!');

            return response()->json($pesan);
        }

        $userpass = $request->get('password');
        $password = $cek_user[0]->Password;

        if (password_verify($userpass, $password)) {
            Auth::loginUsingId($cek_user[0]->id, $request->get('remember'));
            $pesan = array('status' => 'success', 'authUser' => true, Auth::user());

            return response()->json($pesan);
        } else {
            $pesan = array('status' => 'Password salah!!');
            return response()->json($pesan);
        }
    }

    public function logoutvue()
    {
        Auth::logout();
    }

    public function checkloginvue(Request $request)

    {

        try {
            if (Auth::check()) {
                $pesan = array('status' => 'active', 'authUser' => true, Auth::user());

                return response()->json($pesan);
            } else {
                $pesan = array('status' => 'Inactive', 'authUser' => false);

                return response()->json($pesan);
            }
        } catch (\Exception $e) {
            $pesan = array('status' => 'Inactive', 'authUser' => false);
            return response()->json($pesan);
        }
    }
}
