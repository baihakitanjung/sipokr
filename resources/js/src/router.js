/*=========================================================================================
  File Name: router.js
  Description: Routes for vue-router. Lazy loading is enabled.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
import axiosRetry from 'axios-retry';


Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior() {
        return {
            x: 0,
            y: 0
        }
    },
    routes: [

        {
            // =============================================================================
            // MAIN LAYOUT ROUTES
            // =============================================================================
            path: '',
            component: () => import('./layouts/main/Main.vue'),
            children: [
                // =============================================================================
                // Theme Routes
                // =============================================================================
                {
                    path: '/',
                    name: 'home',
                    meta: {
                        idmenu: 1,
                        requiresAuth: true,
                        breadcrumb: [{
                            title: 'Home',
                            url: '/'
                        }, ],
                        pageTitle: 'Home',
                        // rule: 'editor'
                    },
                    component: () => import('./views/Home.vue')
                },
                {
                    path: '/absen',
                    name: 'absen',
                    meta: {
                        idmenu: 2,
                        requiresAuth: true,
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },

                            {
                                title: 'Absen',
                                active: true,
                                url: '/absen'
                            },
                        ],
                        pageTitle: 'Absen',
                    },
                    component: () => import('./views/Absen.vue')
                },
                {
                    path: '/apps/todo',
                    redirect: '/apps/todo/all',
                    name: 'todo',
                }, {
                    path: '/apps/todo/:filter',
                    component: () => import('./views/pages/todo/Todo.vue'),
                    meta: {
                        idmenu: 3,
                        rule: 'editor',
                        parent: "todo",
                        no_scroll: true,
                        breadcrumb: [{
                                title: 'Home',
                                url: '/'
                            },

                            {
                                title: 'Todo',
                                active: true,
                                url: '/apps/todo'
                            },
                        ],
                        pageTitle: 'Todo',
                    }
                },
            ],
        },
        // =============================================================================
        // FULL PAGE LAYOUTS
        // =============================================================================
        {
            path: '',
            component: () => import('@/layouts/full-page/FullPage.vue'),
            children: [
                // =============================================================================
                // PAGES
                // =============================================================================
                {
                    path: '/login',
                    name: 'page-login',
                    component: () => import('@/views/pages/Login.vue')
                },
                {
                    path: '/pages/error-404',
                    name: 'page-error-404',
                    component: () => import('@/views/pages/Error404.vue')
                },
            ]
        },
        // Redirect to 404 page, if no match found
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})


// export const getUsers = () => {
//     var strr = [];
//     axios
//         .post("/checkloginvue", {})
//         .then(function (response) {
//             strr.push(response.data)
//         })
//         .catch(error => {})
//     return strr;
// }

router.beforeEach((to, from, next) => {
    // const authUser = JSON.parse(window.localStorage.getItem('authUser'));
    axiosRetry(axios, {
        retries: 3
    });
    let checkLogin =
        axios
        .post("/checkloginvue", {})
        .then(response => {
            return response.data;
        })
        .catch(error => {})

    // console.log(makanan);

    const promise = Promise.resolve(checkLogin);

    promise.then(function (val) {

        let authUser = val.authUser;

        // console.log(authUser);
        if (authUser && to.name == 'page-login') {
            next('/')
        } else {
            if (to.matched.some(record => record.meta.requiresAuth)) {
                if (authUser) {
                    // console.log(to.name);
                    next()
                } else if (!authUser) {
                    next({
                        path: '/login',
                        query: {
                            redirect: to.fullPath
                        }
                    })
                } else {
                    next({
                        path: '/login',
                        query: {
                            redirect: to.fullPath
                        }
                    })
                }
            }
        }
    });

    // console.log(test);



    next()
})

router.afterEach(() => {
    // Remove initial loading
    const appLoading = document.getElementById('loading-bg')
    if (appLoading) {
        appLoading.style.display = "none";
    }
})

export default router
