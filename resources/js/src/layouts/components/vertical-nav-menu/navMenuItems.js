/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [{
        url: "/",
        name: "Home",
        slug: "home",
        icon: "HomeIcon",
    },
    {
        url: "/absen",
        name: "Absen",
        slug: "absen",
        icon: "UserIcon",
    },
    {
        url: "/apps/todo",
        name: "Todo",
        slug: "todo",
        icon: "FileIcon",
    },
]
